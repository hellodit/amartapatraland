-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27 Okt 2018 pada 06.35
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amartapatraland`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 'asditaprasetya@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact`
--

CREATE TABLE `contact` (
  `id` int(60) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `tipeapartemen` varchar(255) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact`
--

INSERT INTO `contact` (`id`, `nama`, `email`, `phone`, `tipeapartemen`, `pesan`) VALUES
(1, 'as', 'as@gmail.com', '082243629916', 'Gambar satu', '<p>sasasas</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE `gallery` (
  `img_id` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `img` varchar(60) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`img_id`, `name`, `img`, `description`) VALUES
('5bcd6d568b40b', 'Gambar satu', '5bcd6d568b40b.jpg', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel condimentum metus. Cras porta risus a metus ultrices, sit amet fringilla sapien interdum. Integer fringilla velit vel urna ultrices molestie. Vivamus <strong>dictum</strong> justo ex, ut imperdiet nunc ornare eu. Integer facilisis sagittis elit quis commodo. Mauris at sagittis enim. Cras venenatis sit amet sapien quis pretium. Donec enim quam, pharetra et felis at, malesuada lacinia est. Cras posuere, justo commodo lobortis tincidunt, ante enim dictum sem, non ultricies turpis sem non purus. In viverra venenatis ullamcorper. Proin ullamcorper ornare gravida. Quisque elementum felis mi, ullamcorper tristique dolor scelerisque eu. Maecenas porta finibus finibus. Phasellus volutpat, lorem in rhoncus hendrerit, mauris nulla tristique dolor, eu congue mauris dui <strong>eget</strong> lorem. Morbi laoreet luctus tellus vel aliquet. Nullam in posuere urna, sed dignissim mi.</span></p>'),
('5bcd6e067727e', 'Gambar dua', '5bcd6e067727e.jpg', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel condimentum metus. Cras porta risus a metus ultrices, sit amet fringilla sapien interdum. Integer fringilla velit vel urna ultrices molestie. Vivamus dictum justo ex, ut imperdiet nunc ornare eu. Integer facilisis sagittis elit quis commodo. Mauris at sagittis enim. Cras venenatis sit amet sapien quis pretium. Donec enim quam, pharetra et felis at, malesuada lacinia est. Cras posuere, justo commodo lobortis tincidunt, ante enim dictum sem, non ultricies turpis sem non purus. In viverra venenatis ullamcorper. Proin ullamcorper ornare gravida. Quisque elementum felis mi, ullamcorper tristique dolor scelerisque eu. Maecenas porta finibus finibus. Phasellus volutpat, lorem in rhoncus hendrerit, mauris nulla tristique dolor, eu congue mauris dui eget lorem. Morbi laoreet luctus tellus vel aliquet. Nullam in posuere urna, sed dignissim mi.</span></p>'),
('5bcd6e156682c', 'Gambar tiga', '5bcd6e156682c.jpg', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel condimentum metus. Cras porta risus a metus ultrices, sit amet fringilla sapien interdum. Integer fringilla velit vel urna ultrices molestie. Vivamus dictum justo ex, ut imperdiet nunc ornare eu. Integer facilisis sagittis elit quis commodo. Mauris at sagittis enim. Cras venenatis sit amet sapien quis pretium. Donec enim quam, pharetra et felis at, malesuada lacinia est. Cras posuere, justo commodo lobortis tincidunt, ante enim dictum sem, non ultricies turpis sem non purus. In viverra venenatis ullamcorper. Proin ullamcorper ornare gravida. Quisque elementum felis mi, ullamcorper tristique dolor scelerisque eu. Maecenas porta finibus finibus. Phasellus volutpat, lorem in rhoncus hendrerit, mauris nulla tristique dolor, eu congue mauris dui eget lorem. Morbi laoreet luctus tellus vel aliquet. Nullam in posuere urna, sed dignissim mi.</span></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `product_id` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`product_id`, `name`, `price`, `image`, `description`) VALUES
('5bd3eaab65509', 'Apatertemen tipe satu', 200000, '5bd3eaab65509.jpg', '<p>jadi ini apartemen tipe satu mas, bagus ga</p>'),
('5bd3eac4c0779', 'Apatertemen tipe dua', 200000, '5bd3eac4c0779.jpg', '<p>kalau ini apartemen tipe dua mas</p>'),
('5bd3ead8647b8', 'Apatertemen tipe tiga', 2000000, '5bd3ead8647b8.jpg', '<p>Ini sih tiga mas</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `user_id`, `created`) VALUES
(25, 'db02b0ca0e7ceff6fae9e341fcc13a', 1, '2018-10-10'),
(26, '4987ed275e1e9b3870d9b1f9f1b887', 1, '2018-10-10'),
(27, '0576d356be4b71c6e5959922f3baa5', 1, '2018-10-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
