<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Email</th>
										<th>Phone Number</th>
										<th>Tipe Apartemen</th>
										<th>Pesan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($contact as $contact): ?>
									<tr>
										<td width="150">
											<?php echo $contact->nama ?>
										</td>
										<td>
											<?php echo $contact->email ?>
										</td>
										<td>
											<?php echo $contact->phone ?>
										</td>
										<td>
											<?php echo $contact->tipeapartemen ?>
										</td>
										<td>
											<?php echo $contact->pesan ?>
										</td>
										<td width="250">
											<a onclick="deleteConfirm('<?php echo site_url('admin/contact/delete/'.$contact->id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
	</script>
	
</body>

</html>