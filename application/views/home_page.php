<?php $this->load->view("_partials/head.php") ?>
<?php $this->load->view("_partials/header.php") ?>
<?php $this->load->view("_partials/navbar.php") ?>

    <div class="site-blocks-cover" style="background-image: url(<?php echo base_url('assets/images/home.jpg')?>);" data-aos="fade">

<div class="container">
  <div class="row align-items-start align-items-md-center justify-content-end">
	<div class="col-md-5 text-center text-md-left pt-5 pt-md-0">
	  <h1 class="mb-2 clr">Amartha Partaland</h1>
	  <div class="intro-text text-center text-md-left">
		<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at iaculis quam. Integer accumsan tincidunt fringilla. </p>
		<p>
		  <a href="<?php echo base_url('contact') ?>" class="btn btn-sm btn-primary">Shop Now</a>
		</p>
	  </div>
	</div>
  </div>
</div>
</div>

<div class="site-section site-section-sm site-blocks-1">
<div class="container">
  <div class="row">

	<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="">
		<div class="icon mr-4 align-self-start">
		<span class="icon-truck"></span>
	  </div>
	  <div class="text">
		<h2 class="text-uppercase">Free Shipping</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at iaculis quam. Integer accumsan tincidunt fringilla.</p>
	  </div>
	</div>


	<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="100">
	  <div class="icon mr-4 align-self-start">
		<span class="icon-refresh2"></span>
	  </div>
	  <div class="text">
		<h2 class="text-uppercase">Free Returns</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at iaculis quam. Integer accumsan tincidunt fringilla.</p>
	  </div>
	</div>
	
	<div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="200">
	  <div class="icon mr-4 align-self-start">
		<span class="icon-help"></span>
	  </div>
	  <div class="text">
		<h2 class="text-uppercase">Customer Support</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at iaculis quam. Integer accumsan tincidunt fringilla.</p>
	  </div>
	</div>

  </div>
</div>
</div>

<div class="site-section site-blocks-2">
<div class="container">
  <div class="row">

	<?php foreach ($product as $product): ?>
		<div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="">
			<a class="block-2-item" href="items/<?php echo $product->product_id ?>">
			<figure class="image">
				<img src="<?php echo base_url('upload/product/'.$product->image)?>" alt="" class="img-fluid">
			</figure>
			<div class="text">
				<span class="text-uppercase">Collections</span>
				<h3><?php echo $product->name ?></h3>
			</div>
			</a>
		</div>
	<?php endforeach; ?>
  </div>
</div>
</div>

<div class="site-section block-3 site-blocks-2 bg-light">
<div class="container">
  <div class="row justify-content-center">
	<div class="col-md-7 site-section-heading text-center py-4">
	  <h2>Unit unggulan</h2>
	</div>
  </div>
  <div class="row align-items-center">
	<div class="col-md-12 col-lg-7 mb-5">
	  <a href="#"><img src="<?php echo base_url('assets/images/paket-premium.jpg')?>" alt="Image placeholder" class="img-fluid rounded"></a>
	</div>
	<div class="col-md-12 col-lg-5 text-center pl-md-5">
	  <h2><a href="#">2 Bedroom Premium</a></h2>
	  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam iste dolor accusantium facere corporis ipsum animi deleniti fugiat. Ex, veniam?</p>
	  <p><a href="<?php echo base_url('contact') ?>" class="btn btn-primary btn-sm">Shop Now</a></p>
	</div>
  </div>
</div>
</div>

<div class="site-section block-8">
	<div class="container">
  	<div class="row justify-content-center  mb-5">
		<div class="col-md-7 site-section-heading text-center pt-3">
	  	<h2>Article & Gallery</h2>
		</div>
  	</div>
		<div class="row">

<?php foreach ($gallery as $gallery): ?>
	<div class="col-md-4">
		<div class="card mb-4 shadow-sm">
			<img class="card-img-top" src="<?php echo base_url('upload/gallery/'.$gallery->img) ?>" alt="Card image cap">
			<div class="card-body">
				<p class="card-text"><?php echo substr($gallery->description,0,200); ?>...</p>
				<div class="d-flex justify-content-between align-items-center">
					<div class="btn-group">
						<a href="detail/<?php echo $gallery->img_id ?>" class="btn btn-sm btn-outline-secondary">Detail</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>

</div>
		</div>
</div>



<div class="site-section block-8 bg-light">
	<div class="container">
  	<div class="row justify-content-center  mb-5">
		<div class="col-md-7 site-section-heading text-center pt-3">
	  	<h2>Video</h2>
		</div>
  	</div>
			<video width="100%" height="100%" controls>
  		<source src="<?php echo base_url('assets/images/video.mp4')?>" type="video/mp4">
		</video>
		</div>
</div>

<div class="site-section block-3 site-blocks-2 ">
<div class="container">
  <div class="row justify-content-center">
	<div class="col-md-7 site-section-heading text-center py-4">
	  <h2>Location & Accessibility</h2>
	</div>
  </div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15814.082340316625!2d110.3781527!3d-7.7344714!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x532c76d444b5102!2sAmarta+Apartment!5e0!3m2!1sid!2sid!4v1539700571270" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</div>
</div>


<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

</body>
</html>
