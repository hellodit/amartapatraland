<?php $this->load->view("_partials/head.php") ?>
<?php $this->load->view("_partials/header.php") ?>
<?php $this->load->view("_partials/navbar.php") ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">About</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section border-bottom" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6">
            <div class="block-16">
                <img src="<?php echo base_url('upload/gallery/'.$detail->img) ?>" alt="Image placeholder" class="img-fluid rounded">
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-5">
            <div class="site-section-heading pt-3 mb-4">
              <h2 class="text-black"><?php echo $detail->name?></h2>
            </div>
            <p><?php echo $detail->description?></p>
            <a href="<?php echo base_url('contact') ?>" class="btn btn-primary btn-sm">Shop Now</a>
          </div>
        </div>
      </div>
    </div>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>

  </body>
</html>
