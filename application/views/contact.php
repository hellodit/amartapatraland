<?php $this->load->view("_partials/head.php") ?>
<?php $this->load->view("_partials/header.php") ?>
<?php $this->load->view("_partials/navbar.php") ?>
<div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Contact</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-3 text-black">Get In Touch</h2>
          </div>
          <div class="col-md-7">
            
          <?php if($this->session->flashdata('sucessPesan')):?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('sucessPesan');?>
            </div>
           <?php endif; ?>

          <?php if($this->session->flashdata('failedPesan')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('failedPesan');?>
            </div>
           <?php endif; ?>

            <form action="<?php echo base_url('contactdo');?>" method="post" enctype="multipart/form-data">

              <div class="p-3 p-lg-5 border">

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">Nama<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="nama">
                  </div>
                </div>
                
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_email" class="text-black">Email </label>
                    <input type="email" class="form-control" id="c_email" name="email" placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_email" class="text-black">Phone number <span class="text-danger">*</span></label>
                    <input type="tel" class="form-control" id="c_email" name="phone" placeholder="">
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_email" class="text-black">Tipe Apartment<span class="text-danger">*</span></label>

                      <select class="form-control" name="tipeapartemen">
                        <?php foreach ($product as $product): ?>
                        <option value="<?php echo $product->name?>"><?php echo $product->name?></option>
                        <?php endforeach; ?>
                      </select>

                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_message" class="text-black">Message </label>
                    <textarea name="pesan" id="c_message" cols="30" rows="7" class="form-control"></textarea>
                  </div>
                </div>
                
                <div class="form-group row">
                  <div class="col-lg-12">
                  <input class="btn btn-success" type="submit" name="btn" value="Save" />
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-5 ml-auto">
            <div class="p-4 border mb-3">
              <span class="d-block text-primary h6 text-uppercase"><b>Amarta Apartment</b></span>
              <p class="mb-0">Jl. Palagan Tentara Pelajar No.KM. 7.5, Mudal, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581</p>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php $this->load->view("_partials/footer.php") ?>
<?php $this->load->view("_partials/js.php") ?>
</body>
</html>
