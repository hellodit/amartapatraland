<?php $this->load->view("_partials/head.php") ?>
<?php $this->load->view("_partials/header.php") ?>
<?php $this->load->view("_partials/navbar.php") ?>

    <main role="main">
      <div class="album py-5 bg-light">
        <div class="container">
          <div class="row">


          <?php foreach ($gallery as $gallery): ?>

            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" src="<?php echo base_url('upload/gallery/'.$gallery->img) ?>" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><?php echo substr($gallery->description,0,200); ?>...</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <a href="detail/<?php echo $gallery->img_id ?>" class="btn btn-sm btn-outline-secondary">Detail</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <?php endforeach; ?>

          </div>
        </div>
      </div>

    </main>
    <?php $this->load->view("_partials/footer.php") ?>
    <?php $this->load->view("_partials/js.php") ?>

</body>
</html>
