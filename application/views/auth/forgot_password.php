<!DOCTYPE html>
<html lang="en">

<?php $this->load->view("admin/_partials/head.php") ?>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Reset Password</div>
        <div class="card-body">

          <?php if($this->session->flashdata('usernotfound')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('usernotfound');?>
            </div>
           <?php endif; ?>

          <?php if($this->session->flashdata('successreset')):?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('successreset');?>
            </div>
           <?php endif; ?>


          <div class="text-center mb-4">
            <h4>Forgot your password?</h4>
            <p>Enter your email address and we will send you instructions on how to reset your password.</p>
          </div>

          <form action="<?php echo site_url('admin/auth/forgotpassword')?>" method="POST">
            <div class="form-group">
              <div class="form-label-group">
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Enter email address" required="required" autofocus="autofocus">
                <label for="inputEmail">Enter email address</label>
              </div>
            </div>
            <input class="btn btn-primary btn-block" type="submit"/>
          </form>

          <div class="text-center">
            <a class="d-block small mt-3" href="register.html">Register an Account</a>
            <a class="d-block small" href="login.html">Login Page</a>
          </div>

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <?php $this->load->view('admin/_partials/js.php')?>
    
  </body>

</html>
