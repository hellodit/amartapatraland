<!DOCTYPE html>
<html lang="en">

 <?php $this->load->view("admin/_partials/head.php") ?>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">

          <?php if($this->session->flashdata('failedLogin')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('failedLogin');?>
            </div>
           <?php endif; ?>

          <?php if($this->session->flashdata('invalidToken')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('invalidToken');?>
            </div>
          <?php endif; ?>

          <?php if($this->session->flashdata('updatePassGagal')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('updatePassGagal');?>
            </div>
          <?php endif; ?>

          <?php if($this->session->flashdata('updatePassBerhasil')):?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('updatePassBerhasil');?>
            </div>
          <?php endif; ?>

          <form action="<?php echo site_url('admin/auth/login')?>" method="POST">
            <div class="form-group">
              <div class="form-label-group">

                <input type="text" id="inputUsername" name="username" 
                class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>" 
                placeholder="Username address" required="required" autofocus="autofocus">

                <label for="inputUsername">Username</label>
                <div class="invalid-feedback">
									<?php echo form_error('username') ?>
								</div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="inputPassword" name="password" class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>" placeholder="Password" required="required">
                <label for="inputPassword">Password</label>
                <div class="invalid-feedback">
									<?php echo form_error('password') ?>
								</div>
              </div>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="remember-me">
                  Remember Password
                </label>
              </div>
            </div>
            <input class="btn btn-success" type="submit"/>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="register.html">Register an Account</a>
            <a class="d-block small" href="<?php echo base_url('forgotpassword')?>">Forgot Password?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <?php $this->load->view('admin/_partials/js.php')?>

  </body>

</html>
