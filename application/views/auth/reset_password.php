<!DOCTYPE html>
<html lang="en">

<?php $this->load->view("admin/_partials/head.php") ?>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Reset Password</div>
        <div class="card-body">

          <?php if($this->session->flashdata('usernotfound')):?>
            <div class="alert alert-danger" role="alert">
              <?php echo $this->session->flashdata('usernotfound');?>
            </div>
           <?php endif; ?>


          <div class="text-center mb-4">
            <h4>Forgot your password?</h4>
            <p>Enter your email address and we will send you instructions on how to reset your password.</p>
          </div>

          <form action="<?php echo site_url('resetpassword/token/'.$token)?>" method="POST">
            
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" name="password" id="inputPassword" class="form-control" 
                placeholder="Enter Password" required="required" autofocus="autofocus">
                <label for="inputPassword">Enter Password</label>
                <div class="invalid-feedback">
									<?php echo form_error('password') ?>
								</div>

              </div>
            </div>

            <div class="form-group">
              <div class="form-label-group">
                <input type="password" name="confirmPass" id="inputConfirmPass" class="form-control" 
                placeholder="Enter Confirmation Password" required="required" autofocus="autofocus">
                <label for="inputConfirmPass">Enter Confirmation Password</label>
                <div class="invalid-feedback">
									<?php echo form_error('confirmPass') ?>
								</div>
              </div>
            </div>

            <input class="btn btn-primary btn-block" type="submit"/>
          </form>

          <div class="text-center">
            <a class="d-block small mt-3" href="register.html">Register an Account</a>
            <a class="d-block small" href="login.html">Login Page</a>
          </div>

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <?php $this->load->view('admin/_partials/js.php')?>
    
  </body>

</html>
