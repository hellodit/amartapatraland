<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->model('product_model');
		$this->load->model('gallery_model');
		$data["gallery"] = $this->gallery_model->getAll();
		$data["product"] = $this->product_model->getAll();
		$this->load->view('home_page',$data);

	}

	public function about(){
		$this->load->view('about');
	}

	public function contact(){
		
		$this->load->model('product_model');

		$data["product"] = $this->product_model->getAll();
		$this->load->view('contact',$data);
	}

	public function contactdo(){
		$this->load->model('contact_model');
		$this->load->library('form_validation');
		
    	$contact = $this->contact_model;
        $validation = $this->form_validation;
        $validation->set_rules($contact->rules());

        if($validation->run()){
            $contact->save();
			$this->session->set_flashdata('sucessPesan','Pesan berhasil dikirim');
			redirect('contact');
		}else{
			$this->session->set_flashdata('failedPesan','Pesan gagal dikirim');
			redirect('contact');
		}

	}

	public function gallery()
	{
		$this->load->model('gallery_model');
		$data["gallery"] = $this->gallery_model->getAll();
		$this->load->view('gallery',$data);
	}

	public function detail($img_id = null)
	{
		if(!isset($img_id)) redirect('gallery');
		$img_id = $this->uri->segment(2);
		$this->load->model('gallery_model');
		$data['detail'] = $this->gallery_model->getById($img_id);
		$this->load->view('detail',$data);
	}

	public function items($product_id = null)
	{
		if(!isset($product_id)) redirect(base_url());
		$product_id = $this->uri->segment(2);
		$this->load->model('product_model');

		$data['product'] = $this->product_model->getById($product_id);
		$this->load->view('items',$data);
	}
}
