<?php 
    class Auth extends CI_Controller{
        
        public function __construct(){
            parent::__construct();
            $this->load->model('auth_model');
            $this->load->library('form_validation');
            
        }
        
        public function login(){
            $auth = $this->auth_model;

            $validation = $this->form_validation;
            $validation ->set_rules($auth->rules());

            if($validation->run()){
                $auth->login();
            }

            $this->load->view("auth/login");

        }

        public function logout(){
            $this->session->sess_destroy();
            redirect(base_url('login'));
        }
        
        private function base64url_encode($data) {   
            return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');   
        }   
           
        private function base64url_decode($data) {   
            return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));   
        }

        private function sendMail($message,$emailTo){

            $config =[
                'useragent' => 'CodeIgniter',
                'protocol' => 'smtp',
                'smtp_host' => "ssl://smtp.gmail.com",
                'smtp_port' => "465",
                'smtp_user'=> "",
                'smtp_pass'=> "",
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'validate'  => TRUE,
                'crlf'      => "\r\n",
                'newline'   => "\r\n",
            ];

            $this->load->library('email');

            $this->email->initialize($config);

            $this->email->from($config['smtp_user']);
            $this->email->to($emailTo);

            $this->email->subject('Email password');
            $this->email->message($message);
            $this->email->send();
        }

        public function forgotPassword(){
            $auth = $this->auth_model;
            $this->form_validation->set_rules('email', 'email', 'required|valid_email');
            
            if($this->form_validation->run()){
                $email = $this->input->post('email');
                $clean = $this->security->xss_clean($email);
                $userInfo = $this->auth_model->getUserByEmail($clean);
                
                if(!$userInfo){
                    $this->session->set_flashdata('usernotfound','Email tidak ditemukan');
                    redirect(site_url('forgotpassword'));   
                }

                $token = $this->auth_model->insertToken($userInfo->id);
                $qstring = $this->base64url_encode($token);
                $url = site_url() . 'resetpassword/token/' . $qstring;  
                $link = '<a href="' . $url . '">' . $url . '</a>';
                
                $message = '';             
                $message .= '<strong>Hai, anda menerima email ini karena ada permintaan untuk memperbaharui  
                    password anda.</strong><br>';  
                $message .= '<strong>Silakan klik link ini:</strong> ' . $link;

                $this->sendMail($message,$clean);
      
                $this->session->set_flashdata('successreset','Silahkan periksa email anda');
                redirect(site_url('forgotpassword'));   
            }
            $this->load->view('auth/forgot_password');
        }

        public function resetPassword(){
            $token = $this->base64url_decode($this->uri->segment(3));
            $cleanToken = $this->security->xss_clean($token);
            
            $userInfo = $this->auth_model->isTokenValid($cleanToken);

            if(!$userInfo){
                $this->session->set_flashdata('invalidToken','Token tidak ditemukan atau kedaluarsa');
                redirect(site_url('login'));
            }

            $data = array(
                'username'=>$userInfo->username,
                'email'=>$userInfo->email,
                'token'=>$this->base64url_encode($token)
            );

            $this->form_validation->set_rules('password','Password','required|min_length[5]');
            $this->form_validation->set_rules('confirmPass','Password Confirmation','required|matches[password]');

            if($this->form_validation->run()==FALSE){
                $this->load->view('auth/reset_password',$data);
            }else{
                $post = $this->input->post(NULL,TRUE);
                $cleanPost = $this->security->xss_clean($post);

                $hashed = md5($cleanPost['password']);
                $cleanPost['password'] = $hashed;
                $cleanPost['id_user'] = $userInfo->id;

                unset($cleanPost['confirmPass']);

                if(!$this->auth_model->updatePassword($cleanPost)){  
                    $this->session->set_flashdata('updatePassGagal', 'Update password gagal.');  
                  }else{  
                    $this->session->set_flashdata('updatePassBerhasil', 'Password anda sudah  
                      diperbaharui. Silakan login.');  
                  }  
                redirect(site_url('login'));         
                }  
        }

}

?>