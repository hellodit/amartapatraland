<?php
    class Contact extends CI_Controller{
        public function __construct()
        {
            parent::__construct();   
            if($this->session->userdata('status')!="login"){
                redirect(base_url('login'));
            }

            $this->load->model('contact_model');
            $this->load->library('form_validation');
        }

        public function index(){
            $data["contact"] = $this->contact_model->getAll();
            $this->load->view("admin/contact/list",$data);
        }

        // public function add(){
        //     $gallery = $this->contact_model;
        //     $validation = $this->form_validation;
        //     $validation->set_rules($gallery->rules());

        //     if($validation->run()){
        //         $gallery->save();
        //         $this->session->set_flashdata('sucessGallery','Gamber Telah berhasil ditambahkan');
        //     }
        //     $this->load->view("admin/gallery/new_form");
        // }


        public function delete($id=null){
            if(!isset($id)) show_404();

            if($this->gallery_model->delete($id)){
                redirect(site_url('admin/contact'));
            }
        }


    }

?>