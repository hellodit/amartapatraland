<?php
    class Gallery extends CI_Controller{
        public function __construct()
        {
            parent::__construct();   
            if($this->session->userdata('status')!="login"){
                redirect(base_url('login'));
            }

            $this->load->model('gallery_model');
            $this->load->library('form_validation');
        }

        public function index(){
            $data["gallery"] = $this->gallery_model->getAll();
            $this->load->view("admin/gallery/list",$data);
        }

        public function add(){
            $gallery = $this->gallery_model;
            $validation = $this->form_validation;
            $validation->set_rules($gallery->rulesGallery());

            if($validation->run()){
                $gallery->save();
                $this->session->set_flashdata('sucessGallery','Gamber Telah berhasil ditambahkan');
            }
            $this->load->view("admin/gallery/new_form");
        }

        public function edit($id=null){
            if(!isset($id)) redirect('admin/gallery');

            $gallery = $this->gallery_model;
            $validation = $this->form_validation;
            $validation->set_rules($gallery->rulesGallery());

            if($validation->run()){
                $gallery->update();
                $this->session->set_flashdata('successGallery','Gambar telah berhasil di sunting!');
            }

            $data['gallery'] = $gallery->getById($id);

            if(!$data['gallery']) show_404();
            $this->load->view('admin/gallery/edit_form',$data);
        }

        public function delete($id=null){
            if(!isset($id)) show_404();

            if($this->gallery_model->delete($id)){
                redirect(site_url('admin/gallery'));
                $this->session->set_flashdata('successGalleryDel','Gambar telah berhasil di Hapus!');
            }
        }


    }

?>