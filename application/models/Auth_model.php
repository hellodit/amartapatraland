<?php
    defined('BASEPATH') OR exit('no direct script access allowed');
    
    class auth_model extends CI_model{

        private $_table = "admin";
        private $_token = "tokens";


        public function rules(){
            return[
                ['field' => 'username',
                'label' => 'username',
                'rules' => 'required'],
                
                ['field' => 'password',
                'label' => 'password',
                'rules' => 'required'],
            ];
        }
        
        public function login(){
            $post = $this->input->post();
            $this->username = $post["username"];
            $this->password = $post["password"];

            $where = array(
                'username' => $this->username,
                'password' => md5($this->password)
                );

            $num = $this->db->get_where($this->_table,$where)->num_rows();

            if($num > 0){
                $data_session = array(
                    'username' => $username,
                    'status' => "login"
                );

                $this->session->set_userdata($data_session);
                redirect(base_url("admin"));
            }else{
                $this->session->set_flashdata('failedLogin','Username atau password tidak ditemukan');
            }
        }

        // untuk melakukan reset kode dengan token

        public function getUserInfo($id)  
        {  
          $q = $this->db->get_where($this->_table, array('id' => $id), 1);   
          if($this->db->affected_rows() > 0){  
            $row = $q->row();  
            return $row;  
          }else{  
            error_log('no user found getUserInfo('.$id.')');  
            return false;  
          }  
        }  

        public function getUserByEmail($email){
            $info = $this->db->get_where($this->_table,array('email'=>$email),1);
            if($this->db->affected_rows()>0){
                $row = $info->row();
                return $row;
            }
        }

        public function insertToken($id){
            $token = substr(sha1(rand()),0,30);
            $date = date('Y-m-d');

            $string = array(
                'token'=>$token,
                'user_id'=>$id,
                'created'=>$date  
            );

            $this->db->insert($this->_token, $string);
            return $token.$id;
        }

        public function isTokenValid($token){
             $tkn = substr($token,0,30);  
             $uid = substr($token,30);

            $q = $this->db->get_where($this->_token, array(  
                'token' => $tkn,   
                'user_id' => $uid), 1);

                if($this->db->affected_rows()>0){
                    $row = $q->row();

                    $created = $row->created;
                    $createdTs = strtotime($created);

                    $today = date('Y-m-d');
                    $todayTS = strtotime($today);

                    if($createdTs != $todayTS){  
                        return false;  
                    }
                    $user_info = $this->getUserInfo($row->user_id);
                    
                    return $user_info;
                    
                }else{
                    return false;
                }  
        }
        
        public function updatePassword($post){
            $this->db->where('id',$post['id_user']);
            $this->db->update('admin',array('password' => $post['password']));
            return true;
        }
    }

?>