<?php
    defined('BASEPATH') or exit('NO direct script access allowed');

    class Contact_model extends CI_model{

        private $_table = "contact";

        public function rules(){
            return[
                ['field' => 'nama',
                'label'=>'nama',
                'rules' => 'required'],

                ['field' => 'email',
                'label'=>'email',
                'rules' => 'required'],

                ['field' => 'pesan',
                'label'=>'pesan',
                'rules' => 'required'],
            ];
        }

        public function getAll(){
            return $this->db->get($this->_table)->result();
        }

        public function getById($id){
            return $this->db->get_where($this->_table,["id" => $id])->row();
        }

        public function save(){
            $post = $this->input->post();
            $this->nama = $post["nama"];
            $this->email = $post["email"];
            $this->phone = $post["phone"];
            $this->tipeapartemen = $post["tipeapartemen"];
            $this->pesan = $post["pesan"];
            // $this->img = $this->_uploadImage();
            $this->db->insert($this->_table, $this);
        }

        public function delete($id)
        {
            // $this->_deleteImage($img_id);
            return $this->db->delete($this->_table, 
            array("id" => $id));
        }





    }

?>