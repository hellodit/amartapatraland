<?php
    defined('BASEPATH') or exit('NO direct script access allowed');

    class Gallery_model extends CI_model{
        private $_table = "gallery";

        public $img_id;
        public $name;
        public $description;

        public function rulesGallery(){
            return[
                ['field' => 'name',
                'label'=>'Name',
                'rules' => 'required'],

                ['field' => 'description',
                'label'=>'Description',
                'rules' => 'required'],
            ];
        }

        public function getAll(){
            return $this->db->get($this->_table)->result();
        }

        public function getById($img_id){
            return $this->db->get_where($this->_table,["img_id" => $img_id])->row();
        }


        public function save(){
            $post = $this->input->post();
            $this->img_id = uniqid();
            $this->name = $post["name"];
            $this->description = $post["description"];
            $this->img = $this->_uploadImage();
            $this->db->insert($this->_table, $this);
        }

        public function update(){
            $post = $this->input->post();
            $this->img_id = $post["img_id"];
            $this->name = $post["name"];

            if(!empty($_FILES["image"]["name"])){
                $this->img = $this->_uploadImage();
            }else{
                $this->img = $post["old_image"];
            }

            $this->description = $post["description"];
            $this->db->update($this->_table, $this, array('img_id'=> $post['img_id']));

        }

        public function delete($img_id)
        {
            $this->_deleteImage($img_id);
            return $this->db->delete($this->_table, array("img_id" => $img_id));
        }

        private function _uploadImage(){
            $config['upload_path']      = './upload/gallery/';
            $config['allowed_types']    = 'gif|jpg|png';
            $config['file_name']        = $this->img_id;
            $config['overwrite']        = true;
            $config['max_size']         = 1024;

            $this->load->library('upload',$config);

            if($this->upload->do_upload('image')){
                return $this->upload->data("file_name");
            }
            return "default.jpg";
        }

        private function _deleteImage($img_id){
            $gallery = $this->getById($img_id);
            if($gallery->img!="default.jpg"){
                $filename = explode(".",$gallery->img)[0];
                return array_map('unlink', glob(FCPATH."upload/gallery/$filename.*"));
            }
        }




    }

?>